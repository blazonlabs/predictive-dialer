/*
 * Copyright 2018 neerajnagi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smscountry;

import com.typesafe.config.ConfigFactory;
import com.typesafe.config.Config;
import java.util.Random;

/**
 *
 * @author neerajnagi
 */
public class Util {
    static Random rn = new Random();
    static Config config = ConfigFactory.load("setup.conf");
    public static void allConfig(){
        System.out.println(ConfigFactory.load().toString());
    }      
    public static double rand(double a , double b){
        return a + (b-a)*rn.nextDouble();
    }
    
    public static int rand(int a, int b){
        return a + rn.nextInt(b - a);
    }
    
    public static int rand(int range[]){
        int total=0;
        for(int i: range)
            total+= i;
        int tmp = rand(1, total);
        System.out.printf("%s %s\n", total, tmp);
        int from = 1;
        int to=-1;
        int j;
       for( j =0 ;j<range.length;j++){ 
            to = from + range[j];
            if(tmp >=from && tmp < to)
                return j+1;
            
            from=to;
        }
        return -1;
    }
    
    public static Config conf(){
        return config;
    }
    public static void main(String args[]){
        int[] arr = {100,0};
        System.out.println( rand(arr ));
        System.out.printf("conf is %s",config.getIntList("customer.range-sec-tolerance").toString());
    }
}
