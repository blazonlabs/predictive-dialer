/*
 * Copyright 2010 david varnes.
 *
 * Licensed under the Apache License, version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at:
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smscountry.transport.message;

import com.smscountry.transport.message.*;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import io.netty.handler.codec.TooLongFrameException;
import com.smscountry.transport.HeaderParser;
import com.smscountry.transport.message.EslHeaders.Name;
import java.nio.ByteBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Decoder used by the IO processing pipeline. Client consumers should never
 * need to use this class.
 * <p/>
 * Follows the following decode algorithm (from FreeSWITCH wiki)
 * <pre>
 *    Look for \n\n in your receive buffer
 *
 *    Examine data for existence of Content-Length
 *
 *    If NOT present, process event and remove from receive buffer
 *
 *    IF present, Shift buffer to remove 'header'
 *    Evaluate content-length value
 *
 *    Loop until receive buffer size is >= Content-length
 *    Extract content-length bytes from buffer and process
 * </pre>
 */
public class EslMessageDecoder {

    /**
     * Line feed character
     */
    static final byte LF = 10;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final int maxHeaderSize = 8192;
    private EslMessage currentMessage;
    private boolean treatUnknownHeadersAsBody = true;

    public EslMessage decode(ByteBuffer buffer) throws Exception {

        EslMessage currentMessage = new EslMessage();
        /*
								*  read '\n' terminated lines until reach a single '\n'
         */
        boolean reachedDoubleLF = false;
        while (!reachedDoubleLF) {
            // this will read or fail
            String headerLine = readToLineFeedOrFail(buffer, maxHeaderSize);
            //log.debug("read header line [{}]", headerLine);
            if (!headerLine.isEmpty()) {
                // split the header line
                String[] headerParts = HeaderParser.splitHeader(headerLine);
                Name headerName = Name.fromLiteral(headerParts[0]);
                if (headerName == null) {
                    currentMessage.addBodyLine(headerLine);
                    currentMessage.isEvent = true;
                    /*if (treatUnknownHeadersAsBody) {
								// cache this 'header' as a body line <-- useful for Outbound client mode
								currentMessage.addBodyLine(headerLine);
							} else {
								throw new IllegalStateException("Unhandled ESL header [" + headerParts[0] + ']');
							}*/
                }
                currentMessage.addHeader(headerName, headerParts[1]);
            } else {
                reachedDoubleLF = true;
            }
            // do not read in this line again

        }
        // have read all headers - check for content-length
        if (currentMessage.hasContentLength()) {

            //System.out.printf("buffer length ----------------- %d\n", currentMessage.getContentLength());
            int contentLength = currentMessage.getContentLength();
            /* byte[] tmp = new byte[contentLength];
                                buffer.get(tmp,0,contentLength);
                                ByteBuffer bodyBytes = ByteBuffer.wrap(tmp);*/
            while (buffer.hasRemaining()) {
                String bodyLine = readLine(buffer, contentLength);
                currentMessage.addBodyLine(bodyLine);
            }

        }
        return currentMessage;

    }

    private String readToLineFeedOrFail(ByteBuffer buffer, int maxLineLegth) throws TooLongFrameException {
        StringBuilder sb = new StringBuilder(64);
        while (buffer.hasRemaining()) {
            // this read might fail
            byte nextByte = buffer.get();
            if (nextByte == LF) {
                return sb.toString();
            } else {
                // Abort decoding if the decoded line is too large.
                if (sb.length() >= maxLineLegth) {
                    throw new TooLongFrameException(
                            "ESL header line is longer than " + maxLineLegth + " bytes.");
                }
                sb.append((char) nextByte);
            }
        }
        return sb.toString();
    }

    private String readLine(ByteBuffer buffer, int maxLineLength) throws TooLongFrameException {
        StringBuilder sb = new StringBuilder(64);
        while (buffer.hasRemaining()) {
            // this read should always succeed
            byte nextByte = buffer.get();
            if (nextByte == LF) {
                return sb.toString();
            } else // Abort decoding if the decoded line is too large.
            if (sb.length() >= maxLineLength) {

                //throw new TooLongFrameException("ESL message line is longer than " + maxLineLength + " bytes.");
            } else {
                sb.append((char) nextByte);
            }
        }

        return sb.toString();
    }

}
