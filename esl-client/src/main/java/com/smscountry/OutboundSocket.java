/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smscountry;

/**
 *
 * @author neerajnagi
 */
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.io.Tcp.Bound;
import akka.io.Tcp.CommandFailed;
import akka.io.Tcp.Connected;
import akka.io.Tcp.Received;
import akka.io.TcpMessage;
import akka.util.ByteString;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import scala.concurrent.duration.Duration;

public class OutboundSocket extends AbstractActor {

    final ActorSystem system = getContext().getSystem();
    final ActorRef manager;
    int listenPort;
    int count;
    HashMap<String, ActorRef> callLegs = new HashMap<String, ActorRef>();
    
    public OutboundSocket(ActorRef manager, int listenPort) {//, ActorRef handler) {
        System.out.println("OutboundSocket Constructor Start");
        this.manager = manager;
        this.listenPort = listenPort;
        System.out.println("OutboundSocket Constructor End");
    }

    public static Props props(ActorRef manager) {
        return Props.create(OutboundSocket.class, manager);
    }

    @Override
    public void preStart() throws Exception {
        System.out.println("OutboundSocket PreStart Start");
         manager.tell(TcpMessage.bind(getSelf(), new InetSocketAddress(this.listenPort), 100), getSelf());

        //system.scheduler().schedule(Duration.Zero(), Duration.create(30, TimeUnit.SECONDS  ), getSelf(), "SEND_PING", system.dispatcher(), null);
        System.out.println("OutboundSocket PreStart End");
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(Received.class, msg -> {
                    
                    System.out.println( "Responder Recieved.class => \n" +new String(msg.data().toArray(), "UTF-8") );
                    
                    getSender().tell(TcpMessage.write(ByteString.fromString("connect\n\n", "UTF-8")), getSelf());

                })

                .match(Bound.class, message -> {
                    System.out.println("OutboundSocket Bound.class => " + message);
                    manager.tell(message, getSelf());
                })
                .match(CommandFailed.class, message -> {
                    System.out.println("OutboundSocket CommandFailed.class => " + message);
                    getContext().stop(getSelf());
                })
                .match(Connected.class, conn -> {
                    this.count=this.count+1;
                    System.out.println("OutboundSocket Connected.class => " + conn.toString() + "  " + count);
                    manager.tell(conn, getSelf());
                    final ActorRef handler = getContext().actorOf(Props.create(CallLeg.class, manager, constants.Constants.RESPONDER));
                    callLegs.put(conn.toString(), handler);
                    getSender().tell(TcpMessage.register(handler), getSelf());
                    //handler.tell("INIT_SENDER", getSender());
                    getSender().tell(TcpMessage.write(ByteString.fromString("connect\n\n", "UTF-8")), getSelf());
                    //getSender().tell(TcpMessage.write(ByteString.fromString("linger\n\n", "UTF-8")), getSelf());
                })

                .matchAny(msgAny -> {
                    try{
                        System.out.println("OutboundSocket msgAny => " + msgAny);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .build();
    }

}
