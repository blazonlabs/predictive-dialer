/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smscountry;

/**
 *
 * @author neerajnagi
 */
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.io.Tcp.Bound;
import akka.io.Tcp.CommandFailed;
import akka.io.Tcp.Connected;
import akka.io.Tcp.Received;
import akka.io.TcpMessage;
import akka.util.ByteString;
import com.smscountry.msg.Customer;
import com.smscountry.transport.message.EslEvent;
import com.smscountry.transport.message.EslEventHeaderNames;
import com.smscountry.transport.message.EslHeaders;
import com.smscountry.transport.message.EslMessage;
import com.smscountry.transport.message.EslMessageDecoder;
import com.typesafe.config.Config;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;
import scala.Array;
import scala.concurrent.duration.Duration;

public class InboundSocket extends AbstractActor {

    final ActorSystem system = getContext().getSystem();
    final ActorRef manager;
    ActorRef freeSwitch;
    ActorRef master;
    String remoteIP;
    int remotePort;
    int count;
    String prvMsg = "";
    EslMessageDecoder emd = new EslMessageDecoder();
    byte[] arrier=null;
    
    Config conf = Util.conf();
    String prefix = conf.getString("customer.prefix");
    //HashMap<String, ActorRef> callLegs = new HashMap<String, ActorRef>();
    
   /* public void extractJSON(String o){
        //System.out.println("extractJSON  "  + o);
         
        int sindx = o.indexOf("{");
        int eindx = o.indexOf("}")+1;
        
        if(sindx > 0 && eindx > 0 && eindx > sindx){
            //System.out.println("------------------------------> : "+o.substring(sindx, eindx));
            getSelf().tell(new JSONObject(o.substring(sindx, eindx)), getSelf());
            if(o.lastIndexOf("}") > eindx)
                extractJSON(o.substring(eindx+1));
        }
    }*/
//    private byte[] _doubleLF = {0x10,0x10};
//    private ByteString doubleLF = ByteString.fromArray(_doubleLF);
    
    private byte[] concatenate(byte[] a, byte[] b){
        if(a==null && b==null)
            return new byte[0];
        else if(a==null && b!=null)
            return b;
        else if(b==null && a!=null)
            return a;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        try{
            outputStream.write( a );
            outputStream.write( b );
        }finally {
            return outputStream.toByteArray( );
        }
    }
    
    public int occurrencesOfSubstring(String str, String findStr){
        int lastIndex = 0;
        int count = 0;

        while(lastIndex != -1){

            lastIndex = str.indexOf(findStr,lastIndex);

            if(lastIndex != -1){
                count ++;
                lastIndex += findStr.length();
            }
        }
        return count;
    }
    
    public void extractData(byte[] bytes) throws Exception{
        //System.out.println("------------------------extractData Start------------------------");
        byte[] b = bytes;
        int counter = 0;
        String doubleLF = new String(new byte[]{10,10}, "UTF-8");
        if(b.length>0) {
            //System.out.println(">>>>>>>>>>\n" + new String(b) + "\n<<<<<<<<<<\n");
            //b = concatenate(arrier,b);
            String message = new String(b, "UTF-8");
            int noOfDLF = occurrencesOfSubstring(message, doubleLF);
            //System.out.println("noOfDLF : "+noOfDLF);
            
            String[] messages = message.split(doubleLF);
            for(int i=0;i<messages.length;i++){
                String msg = messages[i];//+"\n";
                
//                byte[] temp = msg.getBytes();
//                for(int j=0;j<temp.length;j++) {
//                    System.out.printf("%x ",temp[j]);
//                }
//                System.out.println();
                
                if(msg.contains("Content-Length") && msg.contains("Content-Type") || msg.contains("Content-Length")){
                    //System.out.println("-------- ESL ----- : contains length");
                    prvMsg = msg+"\n";
                    //EslMessage emsg = new EslMessageDecoder().decode(ByteBuffer.wrap(msg.getBytes()));
                    //System.out.println("-------- ESL ----- : emsg content length : "+emsg.getContentLength());
                }else if(msg.contains("Content-Type")){
                    //System.out.println("-------- ESL ----- : contains type");
                    EslMessage emsg = emd.decode(ByteBuffer.wrap(msg.getBytes()));
                    getSelf().tell(emsg, getSelf());
                    //System.out.println("-------- ESL ----- : emsg : "+emsg.toString());
                }else{
                    //System.out.println("-------- ESL ----- : else case");
                    //msg = prvMsg + msg;
                    if(!(message.lastIndexOf(doubleLF) == (b.length-2)) && i == messages.length-1){
                        prvMsg = prvMsg + msg;
                    }else{
                        msg = prvMsg + msg;
                        EslMessage emsg = emd.decode(ByteBuffer.wrap(msg.getBytes()));
                       getSelf().tell(emsg, getSelf());
                        //System.out.println("-------- ESL ----- : emsg : "+emsg.toString());
                        prvMsg = "";
                        //for debugging
//                        if(emsg.isEvent){
//                            EslEvent eve = new EslEvent(emsg);
//                            try{
//                                if(eve.getEventName() != null){
//                                    System.out.printf("DEBUG : EVENT %s -> RAW MSG -----> %s\n",eve.getEventName(),msg);
////                                    if(eve.getEventName().equalsIgnoreCase("CHANNEL_BRIDGE") ){//&& eve.getEventHeaders().get("Caller-Direction").equals("outbound")){
////                                        System.out.printf("DEBUG : EVENT channel_park RAW MSG -----> %s\n",msg);
////                                    }
////                                    if(eve.getEventName().equalsIgnoreCase("CHANNEL_UNBRIDGE") ){//&& eve.getEventHeaders().get("Caller-Direction").equals("outbound")){
////                                        System.out.printf("DEBUG : EVENT CHANNEL_HANGUP_COMPLETE RAW MSG -----> %s\n",msg);
////                                    }
//                                }
//                            }catch(Exception e){
//                                e.printStackTrace();
//                            }
//                        }
                    }
                }
            }
                
//            byte x, y;
//            int start, end;
//            x = b[0];
//            start = 0;
//            for(int i=1;i<b.length;i++) {
//                end = i; 
//                y = b[i];
//                //System.out.printf("%x ",x);
//                if(x==0x0a && y==0x0a) {
//                    String msg = new String(Arrays.copyOfRange(b,start,end));
//                    //System.out.println("------------------------>>>>>>>>>> in for loop tell : "+msg);
//                    if(!msg.contains("Content-Length") && counter == 0){
//                        msg = prvMsg + msg;
//                    }
//                    System.out.printf("x => %x ",x);
//                    System.out.printf("y => %x ",y);
//                    //getSelf().tell(msg, getSelf());
//                    start = i;
//                    counter++;
//                    prvMsg = msg;
//                }
//                x = y;
//            }



//            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//            try {
//                if(arrier != null)
//                    outputStream.write(arrier);
//                outputStream.write(Arrays.copyOfRange(b, start, b.length));
//            }catch (Exception ex) {
//                Logger.getLogger(InboundSocket.class.getName()).log(Level.SEVERE, null, ex);
//            }finally {
//                arrier = outputStream.toByteArray( );
//            }
        }
        //System.out.println("------------------------extractData End------------------------");
    }
    
    public InboundSocket(ActorRef manager, String remoteIP, int remotePort) {//, ActorRef handler) {
        System.out.println("InboundSocket Constructor Start");
        this.manager = manager;
        this.remotePort = remotePort;
        this.remoteIP = remoteIP;
        System.out.println("InboundSocket Constructor End");
    }

    public static Props props(ActorRef manager) {
        return Props.create(InboundSocket.class, manager);
    }

    @Override
    public void preStart() throws Exception {
        System.out.println("InboundSocket PreStart Start");
           manager.tell(TcpMessage.connect(new InetSocketAddress(this.remoteIP, this.remotePort)), getSelf()); 
        //system.scheduler().schedule(Duration.Zero(), Duration.create(30, TimeUnit.SECONDS  ), getSelf(), "SEND_PING", system.dispatcher(), null);
        System.out.println("InboundSocket PreStart End");
    }

    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(Received.class, message -> {
                    //System.out.println( "InboundSocket Recieved.class => \n" + extractJSON(new String(message.data().toArray(), "UTF-8")).toString() );
                    //String str = new String(message.data().toArray(), "UTF-8");
                    //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>> : "+str);
                    extractData(message.data().toArray());
                })
                .match(EslMessage.class, message -> {
                    //System.out.println("InboundSocket EslMessage.class => " + message.toString());
                    //System.out.println("InboundSocket : "+message.getHeaders().toString());
                    //System.out.println(message.getContentType());
                    //System.out.println(message.getBodyLines().size());
                    //System.out.println(Arrays.toString(message.getBodyLines().toArray()));
                    if(message.isEvent){
                        EslEvent eve = new EslEvent(message);
                        try{
                            if(eve.getEventName() != null){
                                System.out.println("InboundSocket : Event : "+eve.getEventName()+", State : "+eve.getEventHeaders().get("Channel-State"));
                                if(eve.getEventName().equalsIgnoreCase("CHANNEL_PARK") && eve.getEventHeaders().get("Caller-Direction").equals("outbound")){
    //                                System.out.printf("EVENT-----> %s %s\n",eve.getEventName(),eve.getEventHeaders().toString());
    //                                String uuid = eve.getEventHeaders().get("Unique-ID");
    //                                System.out.println("uuid : "+uuid);
    //                                String str = "api uuid_broadcast "+uuid+" playback::/usr/local/freeswitch/audio/all-agents-busy.wav aleg\n\n";
    //                                System.out.println("InboundSocket str => " + str);
    //                                freeSwitch.tell(TcpMessage.write(ByteString.fromString(str, "UTF-8")), getSelf());
    //
    //                                str = "api uuid_transfer "+uuid+" 1004\n\n";
    //                                System.out.println("InboundSocket str => " + str);
    //                                //freeSwitch.tell(TcpMessage.write(ByteString.fromString(str, "UTF-8")), getSelf());
                                    master.tell(eve, getSelf());
                                }
//                                if(eve.getEventName().equalsIgnoreCase("channel_park") && eve.getEventHeaders().get("Caller-Direction").equals("inbound")){
//                                    String uuid = eve.getEventHeaders().get("Unique-ID");
//                                    String str = "api uuid_kill "+uuid+"\n\n";
//                                    freeSwitch.tell(TcpMessage.write(ByteString.fromString(str, "UTF-8")), getSelf());
//                                }
                                if(eve.getEventName().equalsIgnoreCase("CHANNEL_HANGUP_COMPLETE") && eve.getEventHeaders().get("Caller-Direction").equals("outbound")){
                                    //System.out.printf("InboundSocket : EVENT-----> %s %s\n",eve.getEventName(),eve.getEventHeaders().toString());
                                    master.tell(eve, getSelf());
                                }
                                if(eve.getEventName().equalsIgnoreCase("CHANNEL_BRIDGE") && eve.getEventHeaders().get("Caller-Direction").equals("outbound")){
                                    //System.out.printf("InboundSocket : EVENT-----> %s %s\n",eve.getEventName(),eve.getEventHeaders().toString());
                                    master.tell(eve, getSelf());
                                }
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        //System.out.printf("EVENT-----> %s %s\n",eve.getEventName(),eve.getEventHeaders().toString());
                    }
                })
                .match(ActorRef.class, ar -> {
                    try{
                        System.out.println("InboundSocket ActorRef.class => " + ar.path().toString());
                        master = ar;
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(Bound.class, message -> {
                    System.out.println("InboundSocket Bound.class => " + message);
                    manager.tell(message, getSelf());
                })
                .match(CommandFailed.class, message -> {
                    System.out.println("InboundSocket CommandFailed.class => " + message);
                    getContext().stop(getSelf());
                })
                .match(ByteString.class, message -> {
                    System.out.println("InboundSocket ByteString.class => " + message);
                })
                .match(Connected.class, conn -> {
                    System.out.println(conn);
                    System.out.println("connected");
                    getSender().tell(TcpMessage.register(getSelf()), getSelf());
                    getSender().tell(TcpMessage.write(ByteString.fromString("auth ClueCon\n\n", "UTF-8")), getSelf());
                    //getSender().tell(TcpMessage.write(ByteString.fromString("api reloadxml\n\n", "UTF-8")), getSelf());
                    getSender().tell(TcpMessage.write(ByteString.fromString("events plain all\n\n", "UTF-8")), getSelf());
                    //getSender().tell(TcpMessage.write(ByteString.fromString("events plain CHANNEL_PARK CHANNEL_BRIDGE CHANNEL_HANGUP_COMPLETE\n\n", "UTF-8")), getSelf());
                    freeSwitch = getSender();
                })
                .match(Customer.class, customer -> {
                    System.out.println("InboundSocket Customer.class => " + customer.getCustomer());
                    String str = "";
                    switch(customer.getStatVariable()){
                        case 1 : customer.setLeadOriginate(System.currentTimeMillis());
                            //str = "api originate loopback/"+customer.getCustomer()+" &park()\n\n";
                            //str = "bgapi originate [stage=originate,hangup_after_bridge=true]loopback/"+customer.getCustomer()+" &park()\n\n";
                            //originate {origination_caller_id_number=04023120011}sofia/gateway/04023120011/9079457917 &park()
                            str = "bgapi originate "+prefix+customer.getCustomer()+" &park()\n\n";
                            break;
                        case 2 : //str = "api uuid_broadcast "+customer.getUuid()+" playback::/usr/local/freeswitch/audio/all-agents-busy.wav aleg\n\n";
                            str = "bgapi uuid_setvar "+customer.getUuid()+" stage parked\n\n";
                            break;
                        case 3 : customer.setAgentOriginate(System.currentTimeMillis());
                            str = "bgapi uuid_transfer "+customer.getUuid()+" bridge-"+customer.getAgent()+"\n\n";
                            break;
                        default : System.out.println("Default Case");
                            break;
                    }
                    if(str != ""){
                        System.out.println("InboundSocket str => " + str);
                        freeSwitch.tell(TcpMessage.write(ByteString.fromString(str, "UTF-8")), getSelf());
                    }
                })
                .matchAny(msgAny -> {
                    try{
                        System.out.println("InboundSocket msgAny => " + msgAny);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .build();
    }

}
