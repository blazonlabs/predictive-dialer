/*
 * Copyright 2018 Abhimanyu.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smscountry.msg;

import com.smscountry.statsdclient.StatsdClient;

/**
 *
 * @author Abhimanyu
 */
public class Customer {
    private String customer;
    private String uuid = "";
    private int statVariable = 0;
    private String agent;
    private long callDuration = 0;
    private long waitToReset = 0;
    private long expiryTS = 0;
    
    private long leadOriginate = 0;     //1
    private long leadAnswer  = 0;       //2
    private long agentOriginate = 0;    //3
    private long agentAnswer  = 0;      //4 //CHANNEL_BRIDGE time
    private long callEndTS = 0;         //5 //CHANNEL_HANGUP_COMPLETE time
    private long postCallEndTS = 0;  
    
    private boolean agentOriginateSuccess;
    
    private boolean hangupComplete;
    
    public Customer(String customer){
        this.customer = customer;
        this.statVariable = 1;
        this.agentOriginateSuccess = true;
        this.hangupComplete = false;
    }

    /**
     * @return the customer
     */
    public String getCustomer() {
        return customer;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the statVariable
     */
    public int getStatVariable() {
        return statVariable;
    }

    /**
     * @param statVariable the statVariable to set
     * Set 1 for "api originate user/$customer &park()\n\n"
     * Set 2 for "api uuid_broadcast $uuid playback::/usr/local/freeswitch/audio/all-agents-busy.wav aleg\n\n"
     * Set 3 for "api uuid_transfer $uuid $agent\n\n"
     */
    public void setStatVariable(int statVariable) {
        this.statVariable = statVariable;
    }

    /**
     * @return the agent
     */
    public String getAgent() {
        return agent;
    }

    /**
     * @param agent the agent to set
     */
    public void setAgent(String agent) {
        this.agent = agent;
    }

    /**
     * @return the callDuration
     */
    public long getCallDuration() {
        return callDuration;
    }

    /**
     * @param callDuration the callDuration to set
     */
    public void setCallDuration(long callDuration) {
        this.callDuration = callDuration;
    }

    /**
     * @return the waitToReset
     */
    public long getWaitToReset() {
        return waitToReset;
    }

    /**
     * @param waitToReset the waitToReset to set
     */
    public void setWaitToReset(long waitToReset) {
        this.waitToReset = waitToReset;
    }

    /**
     * @return the expiryTS
     */
    public long getExpiryTS() {
        return expiryTS;
    }

    /**
     * @param expiryTS the expiryTS to set
     */
    public void setExpiryTS(long expiryTS) {
        this.expiryTS = expiryTS;
    }
    
    /**
     * @return the callEndTS
     */
    public long getCallEndTS() {
        return callEndTS;
    }

    /**
     * @param callEndTS the callEndTS to set
     */
    public void setCallEndTS(long callEndTS) {
        this.callEndTS = callEndTS;
        StatsdClient.statsd.recordGaugeValue(String.format("customer_ts,customer=%s",customer), 5.0);
    }

    /**
     * @return the postCallEndTS
     */
    public long getPostCallEndTS() {
        return postCallEndTS;
    }

    /**
     * @param postCallEndTS the postCallEndTS to set
     */
    public void setPostCallEndTS(long postCallEndTS) {
        this.postCallEndTS = postCallEndTS;
    }

    /**
     * @return the leadOriginate
     */
    public long getLeadOriginate() {
        return leadOriginate;
    }

    /**
     * @param leadOriginate the leadOriginate to set
     */
    public void setLeadOriginate(long leadOriginate) {
        this.leadOriginate = leadOriginate;
        StatsdClient.statsd.recordGaugeValue(String.format("customer_ts,customer=%s",customer), 1.0);
    }

    /**
     * @return the leadAnswer
     */
    public long getLeadAnswer() {
        return leadAnswer;
    }

    /**
     * @param leadAnswer the leadAnswer to set
     */
    public void setLeadAnswer(long leadAnswer) {
        this.leadAnswer = leadAnswer;
        StatsdClient.statsd.recordGaugeValue(String.format("customer_ts,customer=%s",customer), 2.0);
    }

    /**
     * @return the agentOriginate
     */
    public long getAgentOriginate() {
        return agentOriginate;
    }

    /**
     * @param agentOriginate the agentOriginate to set
     */
    public void setAgentOriginate(long agentOriginate) {
        this.agentOriginate = agentOriginate;
        StatsdClient.statsd.recordGaugeValue(String.format("customer_ts,customer=%s",customer), 3.0);
    }

    /**
     * @return the agentAnswer
     */
    public long getAgentAnswer() {
        return agentAnswer;
    }

    /**
     * @param agentAnswer the agentAnswer to set
     */
    public void setAgentAnswer(long agentAnswer) {
        this.agentAnswer = agentAnswer;
        StatsdClient.statsd.recordGaugeValue(String.format("customer_ts,customer=%s",customer), 4.0);
    }

    /**
     * @return the agentOriginateSuccess
     */
    public boolean isAgentOriginateSuccess() {
        return agentOriginateSuccess;
    }

    /**
     * @param agentOriginateSuccess the agentOriginateSuccess to set
     */
    public void setAgentOriginateSuccess(boolean agentOriginateSuccess) {
        this.agentOriginateSuccess = agentOriginateSuccess;
    }

    /**
     * @return the hangupComplete
     */
    public boolean isHangupComplete() {
        return hangupComplete;
    }

    /**
     * @param hangupComplete the hangupComplete to set
     */
    public void setHangupComplete(boolean hangupComplete) {
        this.hangupComplete = hangupComplete;
    }
    
}
