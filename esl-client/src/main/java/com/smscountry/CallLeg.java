/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smscountry;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.io.Tcp;
import akka.io.TcpMessage;
import akka.util.ByteString;
//import io.moquette.interception.messages.InterceptPublishMessage;
import io.netty.buffer.ByteBufUtil;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import com.smscountry.transport.*;
import com.smscountry.transport.message.*;
import io.netty.buffer.Unpooled;
import com.smscountry.dialplan.*;

/**
 *
 * @author Abhimanyu
 */
public class CallLeg extends AbstractActor {
    
    InetSocketAddress remote = null;
    final ActorRef manager;
    Execute tx;
    String peerType;//OutboundSocketCallLeg OR Initiator CallLeg
    boolean firstMsg = true;
    EslMessageDecoder emd = new EslMessageDecoder();
    Callee callee = new Callee();
    
   
    public CallLeg(ActorRef manager,  String peerType) {
        System.out.println("CallLeg Constructor Start");
        this.manager = manager;
        this.peerType = peerType;
        this.tx = new Execute();
        System.out.println("CallLeg Constructor End");
    }
    
    public static Props props(InetSocketAddress remote, ActorRef manager) {
        return Props.create(CallLeg.class, remote, manager);
    }
    
    @Override
    public void preStart() {
        try{
            System.out.println("CallLeg preStart Start");

            System.out.println("CallLeg preStart End");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()

                .match(Tcp.CommandFailed.class, message -> {
                    try{
                        System.err.println("CallLeg CommandFailed.class => " + message);
                        getContext().stop(getSelf());
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(Tcp.Connected.class, conn -> {
                    try{
                        System.out.println("CallLeg Connected.class => " + conn);
                        if(peerType.equals(constants.Constants.INITIATOR)){
                            getSender().tell(TcpMessage.register(getSelf()), getSelf());
                           
                        }
                        if(peerType.equals(constants.Constants.RESPONDER)){
                            manager.tell(conn, getSelf());
                            getSender().tell(TcpMessage.register(getSelf()), getSelf());
                           // getSender().tell(TcpMessage.write(ByteString.fromString("connect\n\n", "UTF-8")), getSelf());
                           
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(Tcp.Received.class, message -> {
                    EslMessage em = emd.decode(message.data().toByteBuffer());
                    EslEvent ee=null;
                    if(em.isEvent){
                    //treat as EslEvent instead of EslMessage
                    ee = new EslEvent(em);
                    //System.out.printf("EVENT-----> %s %s %s",ee.getEventName(),ee.getEventHeaders().toString(),ee.getEventBodyLines().toString());

                    }
                    //else
                    //System.out.printf("MESSAGE-----> %s %s %s",em.getContentType(), em.getHeaders().toString(), em.getBodyLines().toString());
                    //System.out.println("CallLeg Received.class => " + new String(message.data().toArray(),"UTF-8"));
                    if(firstMsg){
                        
                        tx.setRecipient(getSender());
                        firstMsg = false;
                        String destination = ee.getEventHeaders().get("Channel-Destination-Number");
                        //System.err.println("\n\n\n\n\nCallLeg -> Channel-Destination-Number -> "+destination+"\n\n\n\n\n");
                        if(destination.matches("customer.*"))
                            callee = new Customer();
                        else if(destination.matches("agent.*"))
                            callee = new Agent();
                        else if(destination.matches("bridge.*"))
                            callee = new Bridge();
                        else
                            callee = null;
                        
                        if(callee!=null)
                            callee.init(destination, tx);
                        /*tx.preAnswer();
                        tx.sleep("7000");
                        tx.answer();
                        tx.playback("/usr/local/freeswitch/audio/leave-message.wav");
                        tx.hangup();
*/
                            
                    }
                    
                    
                })
                .matchAny(messageAny -> {
                    try{
                        System.out.println("CallLeg msgAny => " + messageAny);
                        //if(messageAny.equals("INIT_SENDER"))
                        //    tx.setRecipient(getSender());
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .build();
        
        
    }
    
  
}