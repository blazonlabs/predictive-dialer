/*
 * Copyright 2018 Abhimanyu.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smscountry;

/**
 *
 * @author Abhimanyu
 */
public class Stats {

    boolean __RMS, __VARIANCE;
    double _RMS, _VARIANCE;
    public org.apache.commons.math3.stat.descriptive.DescriptiveStatistics ds = new org.apache.commons.math3.stat.descriptive.DescriptiveStatistics();
    public org.apache.commons.math3.stat.descriptive.rank.PSquarePercentile p10 = new org.apache.commons.math3.stat.descriptive.rank.PSquarePercentile(10);
    public org.apache.commons.math3.stat.descriptive.rank.PSquarePercentile p25 = new org.apache.commons.math3.stat.descriptive.rank.PSquarePercentile(25);
    public org.apache.commons.math3.stat.descriptive.rank.PSquarePercentile p75 = new org.apache.commons.math3.stat.descriptive.rank.PSquarePercentile(75);
    public org.apache.commons.math3.stat.descriptive.rank.PSquarePercentile p90 = new org.apache.commons.math3.stat.descriptive.rank.PSquarePercentile(90);

    String txt = "";
    public double n, m1, m2, m3, m4, rms, percentile_95, min, max, count = 0;
    public double sumOfSquare;
    public double outlierCount;

    public void add(double _x) {
        count = count + 1;
        if (count == 1) {
            min = _x;
            max = _x;
        }

        if (_x < min) {
            min = _x;
        }
        if (_x > max) {
            max = _x;
        }
        p10.increment(_x);
        p25.increment(_x);
        p75.increment(_x);
        p90.increment(_x);
        double x = _x;
        if (x != _x) {
            outlierCount += 1;
            System.out.println(_x + " -----> " + x);
        }

        if (!Double.isNaN(x)) {
            ds.addValue(x);
            txt = txt + String.valueOf(x);
            sumOfSquare = sumOfSquare + x * x;
            double n1, delta, delta_n, delta_n2, term1;
            n1 = n;
            n = n + 1;
            delta = x - m1;
            delta_n = delta / n;
            delta_n2 = delta_n * delta_n;
            term1 = delta * delta_n * n1;
            m1 += delta_n;
            m4 += term1 * delta_n2 * (n * n - 3 * n + 3) + 6 * delta_n2 * m2 - 4 * delta_n * m3;
            m3 += term1 * delta_n * (n - 2) - 3 * delta_n * m2;
            m2 += term1;
        }
    }

    public double removeOutlier(double x) {

        double _p75 = p75.getResult();
        double _p25 = p25.getResult();
        double iqr = _p75 - _p25;
        if (Double.isNaN(_p25) || Double.isNaN(_p75)) {
            return x;
        }

        if ((x > (_p25 - 1.5 * iqr)) && (x < (_p75 + 1.5 * iqr))) {
            return x;
        } else if (x > (_p25 - 1.5 * iqr)) {
            return p90.getResult();
        } else if (x < (_p75 + 1.5 * iqr)) {
            return p10.getResult();
        }

        return x;
    }

    public double getCount() {
        return count;
    }
    
    public double getMin() {
        return min;
    }
    
    public double getMax() {
        return max;
    }

    public double getSumOfSquare() {
        return sumOfSquare;
    }

    public double getMean() {
        return m1;
    }

    public double getVariance() {
        double _var = m2 / (n - 1);
        if (Double.isNaN(_var) || _var == 0.0) {
            System.out.println(String.format("varianceNaN %s %s %s %s", ds.getN(), m2 / (n - 1), ds.getVariance(), txt));
        }
        return m2 / (n - 1);
    }

    public double getSkewness() {
        return Math.sqrt(n) * m3 / Math.pow(m2, 1.5);
    }

    public double getKurtosis() {
        return (n * m4 / (m2 * m2)) - 3;
    }

    public double getRms() {
        if (!__RMS) {
            _RMS = Math.sqrt(ds.getSumsq() / ds.getN());
            __RMS = true;
        }
        return _RMS;

    }
    
    public double getCrest(){
        return getPercentile(95.0) / getRms();
    }
    
    public double getPercentile(double p){
        return ds.getPercentile(p);
    }
    
    public double getPercentileBy(double p, double by){
        return ds.getPercentile(p) / ds.getPercentile(by);
    }
}
