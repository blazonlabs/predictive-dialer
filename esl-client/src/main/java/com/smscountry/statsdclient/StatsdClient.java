/*
 * Copyright 2018 Abhimanyu.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smscountry.statsdclient;

import com.smscountry.Util;
import com.timgroup.statsd.StatsDClient;
import com.timgroup.statsd.NonBlockingStatsDClient;
import com.typesafe.config.Config;
/**
 *
 * @author Abhimanyu
 */
public class StatsdClient {
    static Config conf = Util.conf();
    static int telegrafPort = conf.getInt("telegraf.port");
    static String telegrafIp = conf.getString("telegraf.ip");
    
    public static final StatsDClient statsd = new NonBlockingStatsDClient("", telegrafIp, telegrafPort);
}
