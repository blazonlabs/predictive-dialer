/*
 * Copyright 2018 neerajnagi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smscountry.dialplan;

import com.smscountry.Util;
import com.smscountry.transport.Execute;
import com.typesafe.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author neerajnagi
 */
public class Agent extends Callee{

    public Execute tx;
    Config conf = Util.conf();
    
  
    int range_sec_call_duration_start = conf.getIntList("agent_customer.range_sec_call_duration").get(0);
    int range_sec_call_duration_end = conf.getIntList("agent_customer.range_sec_call_duration").get(1);
    int ratio_call_post_call_activity = conf.getInt("agent.ratio_call_post_call_activity");
    int range_sec_pre_answer_ringing_start = conf.getIntList("agent.range_sec_pre_answer_ringing").get(0);
    int range_sec_pre_answer_ringing_end = conf.getIntList("agent.range_sec_pre_answer_ringing").get(1);
    String playback = conf.getString("agent.playback");
    String id;
    
    
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    public Agent(){
        
    }
    public void init(String destinationNumber , Execute exe){
        
        tx = exe;
        id = destinationNumber;
             
        log.info("%s -> answered",id);
        tx.ringReady();
        tx.sleep(String.valueOf((Util.rand(range_sec_pre_answer_ringing_start,range_sec_pre_answer_ringing_end))*1000));
        tx.answer();
        //tx.playback(playback,100);
        tx.sleep(String.valueOf((Util.rand(range_sec_call_duration_start,range_sec_call_duration_end))*1000));
        tx.hangup();
        //wait for ratio_call_post_call_activity and send i am active event
        // send IAMAVAIL
  
        
    }
    
    
    public void rx(String msg){
        //event if needed
    }
}
