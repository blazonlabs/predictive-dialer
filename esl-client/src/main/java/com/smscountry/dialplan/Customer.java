/*
 * Copyright 2018 neerajnagi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smscountry.dialplan;

import com.smscountry.Util;
import com.smscountry.transport.Execute;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * 
 * customer {
    percent-pre-answer-rate = 80
    range-sec-pre-answer-ringing = [5, 20]
    percent-answer-rate = 75
    range-sec-tolerance = [3, 10]
    
}

agent {
    ratio-call-post-call-activity = 0.5
}

agent-customer {
    range-sec-call-duration = [60, 120]
}
 * @author neerajnagi
 */
public class Customer extends Callee{
    public Execute tx;
    Config conf = Util.conf();
    
    int percent_pre_answer_rate = conf.getInt("customer.percent_pre_answer_rate");
    int range_sec_pre_answer_ringing_start = conf.getIntList("customer.range_sec_pre_answer_ringing").get(0);
    int range_sec_pre_answer_ringing_end = conf.getIntList("customer.range_sec_pre_answer_ringing").get(1);
    int percent_answer_rate = conf.getInt("customer.percent_answer_rate");
    int range_sec_tolerance_start = conf.getIntList("customer.range_sec_tolerance").get(0);
    int range_sec_tolerance_end = conf.getIntList("customer.range_sec_tolerance").get(1);
    int range_sec_call_duration_start = conf.getIntList("agent_customer.range_sec_call_duration").get(0);
    int range_sec_call_duration_end = conf.getIntList("agent_customer.range_sec_call_duration").get(1);
    String playback = conf.getString("customer.playback");
    String id;
    
    
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    public Customer(){
        
    }
    public void init(String destinationNumber , Execute exe){
        
        tx = exe;
        id = destinationNumber;
             
        int[] ppar = {percent_pre_answer_rate , 100-percent_pre_answer_rate};
        if(Util.rand(ppar) == 2){
            log.info("%s -> pre_ring_ready hangup",id);
            tx.hangup();
        }
        else{
            tx.ringReady();
            tx.sleep(String.valueOf((Util.rand(range_sec_pre_answer_ringing_start,range_sec_pre_answer_ringing_end))*1000));
            int[] par = {percent_answer_rate , 100-percent_answer_rate};
            if(Util.rand(par)==2){
                tx.hangup();
                log.info("%s -> pre_answer hangup",id);

            }
            else
            {
                log.info("%s -> answered",id);
                tx.answer();
                //tx.playback(playback);
                tx.sleep("150000");
                tx.hangup();
            }
        }
        
    }
    
//    public void init(String destinationNumber , Execute exe){
//        
//        tx = exe;
//        id = destinationNumber;
//             
//
//        //log.info("%s -> answered",id);
//        //System.out.println("\n\n\n\n\n Customer -> id -> "+id+"\n\n\n\n\n");
//        
//        
//        tx.ringReady();
////        tx.sleep("3000");
////        tx.hangup();
////        tx.answer();
////        //tx.playback(playback,100);
////        tx.sleep("15000");
////        tx.hangup();
//        //wait for ratio_call_post_call_activity and send i am active event
//        // send IAMAVAIL
//  
//        int[] par = {5 , 95};
//        if(Util.rand(par)==2){
//            tx.sleep("3000");
//            tx.hangup();
//            //log.info("%s -> pre_answer hangup",id);
//
//        }else
//        {
//            //log.info("%s -> answered",id);
//            tx.sleep("3000");
//            tx.answer();
//            tx.sleep("15000");
//            tx.hangup();
//        }
//        
//    }
    
    public void rx(String msg){
        //event if needed
    }
}


