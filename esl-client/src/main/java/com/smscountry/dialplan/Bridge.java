/*
 * Copyright 2018 Abhimanyu.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smscountry.dialplan;

import com.smscountry.Util;
import com.smscountry.transport.Execute;
import com.typesafe.config.Config;


/*
    <action application="export" data="dialed_extension=$1"/>
    <action application="bind_meta_app" data="1 b s execute_extension::dx XML features"/>
    <action application="bind_meta_app" data="2 b s record_session::$${recordings_dir}/${caller_id_number}.${strftime(%Y-%m-%d-%H-%M-%S)}.wav"/>
    <action application="bind_meta_app" data="3 b s execute_extension::cf XML features"/>
    <action application="bind_meta_app" data="4 b s execute_extension::att_xfer XML features"/>
    <!--action application="set" data="ringback=${us-ring}"/>
    <action application="set" data="transfer_ringback=$${hold_music}"/>
    <action application="set" data="call_timeout=30"/-->
    <action application="set" data="hangup_after_bridge=true"/>
    <action application="set" data="continue_on_fail=true"/>
    <action application="hash" data="insert/${domain_name}-call_return/${dialed_extension}/${caller_id_number}"/>
    <action application="hash" data="insert/${domain_name}-last_dial_ext/${dialed_extension}/${uuid}"/>
    <!--action application="set" datdda="called_party_callgroup=${user_data(${dialed_extension}@${domain_name} var callgroup)}"/-->
    <action application="hash" data="insert/${domain_name}-last_dial_ext/${called_party_callgroup}/${uuid}"/>
    <action application="hash" data="insert/${domain_name}-last_dial_ext/global/${uuid}"/>
    <action application="hash" data="insert/${domain_name}-last_dial/${called_party_callgroup}/${uuid}"/>
    <action application="bridge" data="loopback/agent1"/> 
*/


/**
 *
 * @author Abhimanyu
 */
public class Bridge extends Callee{
    public Execute tx;
    String id;
    Config conf = Util.conf();
    String prefix = conf.getString("agent.prefix");
    
    public Bridge(){
        
    }
    
    public void init(String destinationNumber , Execute exe){
        tx = exe;
        id = (destinationNumber.split("bridge-"))[1];
        //System.out.println("\n\n\n\n\n Bridge -> id -> "+id+"\n\n\n\n\n");
        tx.set("hangup_after_bridge", "true");
        tx.set("stage", "bridge");
        tx.sleep("200");
        tx.bridge(prefix+id);
    }
    
}
