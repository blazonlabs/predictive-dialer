/*
 * Copyright 2018 Abhimanyu.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smscountry;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.io.Tcp;
import akka.util.ByteString;
import com.smscountry.msg.Customer;
import com.smscountry.statsdclient.StatsdClient;
import com.smscountry.transport.message.EslEvent;
import com.smscountry.transport.message.EslEventHeaderNames;
import com.typesafe.config.Config;
import io.moquette.interception.messages.InterceptPublishMessage;
import io.netty.buffer.ByteBufUtil;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import scala.concurrent.duration.Duration;

/**
 *
 * @author Abhimanyu
 */
public class Master extends AbstractActor {
    final ActorSystem system = getContext().getSystem();
    ActorRef inbound;
    Config conf = Util.conf();
    HashMap<String, Integer> agents = new HashMap<String, Integer>();
    HashMap<String, Integer> customers = new HashMap<String, Integer>();
    HashMap<String, Customer> customerObjects = new HashMap<String, Customer>();
    Queue<String> agentSet = new LinkedList<String>();
    Queue<String> parkCustomers = new LinkedList<String>();
    
    double ratio_call_post_call_activity = conf.getDouble("agent.ratio_call_post_call_activity");
    final int nThreshold = 1;
    int nThresholdCounter = 0;
    
    int pCounter = 0;
    HashMap<String, Long> agentPTS = new HashMap<String, Long>();
    Stats PERFORMANCE_CUSTOMER_STATS = new Stats();
    Stats PERFORMANCE_AGENT_STATS = new Stats();
    
    Stats ANSWERING_PERCENTAGE_COUNTER_STATS = new Stats();
    Stats ANSWERING_DURATION_STATS = new Stats();
    Stats CALL_HANDLING_DURATION_STATS  = new Stats();
    int N = 0;
    int customerCheckCounter = 0;
    
    public static Props props() {
        return Props.create(Master.class);
    }

    public Master() {
        System.out.println("Master Constructor Start");
        N = 1;
        ANSWERING_PERCENTAGE_COUNTER_STATS.add(1);
        System.out.println("Master Constructor End");
    }

    @Override
    public void preStart() throws Exception {
        System.out.println("Master PreStart Start");
        system.scheduler().schedule(Duration.Zero(), Duration.create(1, TimeUnit.SECONDS  ), getSelf(), "INIT_CUSTOMER", system.dispatcher(), null);
        system.scheduler().schedule(Duration.Zero(), Duration.create(1, TimeUnit.SECONDS  ), getSelf(), "RESET_AGENTS", system.dispatcher(), null);
        System.out.println("Master PreStart End");
    }
    
    @Override
    public AbstractActor.Receive createReceive() {
        return receiveBuilder()
                .match(Tcp.CommandFailed.class, message -> {
                    System.out.println("Master CommandFailed.class => " + message);
                    getContext().stop(getSelf());
                })
                .match(Tcp.Connected.class, message -> {
                    System.out.println("Master Connected.class => " + message);
                })
                .match(InterceptPublishMessage.class, message -> {
                    System.out.println("Master InterceptPublishMessage.class => " + message.getTopicName());
                    if(message.getTopicName().equals("agents/list")){
                        String msg = new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8"));
                        JSONArray arr = new JSONArray(msg);
                        agents.clear();
                        for (int i = 0; i < arr.length(); i++) {
                            agents.put(arr.getString(i), 1);
                            agentPTS.put(arr.getString(i), 0L);
                            StatsdClient.statsd.recordGaugeValue(String.format("agents,agent=%s",arr.getString(i)), 1);
                        }
                    }
                    if(message.getTopicName().equals("agents/count")){
                        String msg = new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8"));
                        int length = Integer.parseInt(msg);
                        agents.clear();
                        for (int i = 0; i < length; i++) {
                            agents.put("agent"+(i+1), 1);
                            agentPTS.put("agent"+(i+1), 0L);
                            StatsdClient.statsd.recordGaugeValue(String.format("agents,agent=%s","agent"+(i+1)), 1);
                        }
                    }
                    if(message.getTopicName().equals("customers/list")){
                        String msg = new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8"));
                        JSONArray arr = new JSONArray(msg);
                        customers.clear();
                        for (int i = 0; i < arr.length(); i++) {
                            customers.put(arr.getString(i), 1);
                            StatsdClient.statsd.recordGaugeValue(String.format("customers,customer=%s",arr.getString(i)), 1);
                        }
                    }
                    if(message.getTopicName().equals("customers/count")){
                        String msg = new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8"));
                        int length = Integer.parseInt(msg);
                        customers.clear();
                        for (int i = 0; i < length; i++) {
                            customers.put("customer"+(i+1), 1);
                            StatsdClient.statsd.recordGaugeValue(String.format("customers,customer=%s","customer"+(i+1)), 1);
                        }
                    }
                    if(message.getTopicName().equals("agent/*")){
                        String msg = new String(ByteBufUtil.getBytes(message.getPayload()), Charset.forName("UTF-8"));
                        String agent = (message.getTopicName().split("/"))[1];
                        if(msg.equals("I_AM_AVAILABLE")){
                            agents.put(agent, 1);
                            StatsdClient.statsd.recordGaugeValue(String.format("agents,agent=%s",agent), 1);
                        }
                    }
                })
                .match(EslEvent.class, message -> {
                    System.out.println("Master EslEvent.class => " + message.toString());
                    try{
                        if(message.getEventName().equalsIgnoreCase("CHANNEL_PARK") && message.getEventHeaders().get("Caller-Direction").equals("outbound")){
                            System.out.printf("Master : CHANNEL_PARK : EVENT : %s %s\n",message.getEventName(),message.getEventHeaders().toString());
                            String uuid = message.getEventHeaders().get("Unique-ID");
                            System.out.println("Master : CHANNEL_PARK : uuid : "+uuid);
                            String vdu = message.getEventHeaders().get("Caller-Destination-Number");
                            System.out.println("Master : CHANNEL_PARK : customer : "+vdu);
                            Customer customer = customerObjects.get(vdu);
                            if(customer != null){// && getAvailableAgentCount() > 0){
                                System.out.println("Master : CHANNEL_PARK : customer : Stat Variable : "+customer.getStatVariable());
                                customer.setUuid(uuid);
                                parkCustomers.add(vdu);
                                customer.setStatVariable(2);
                                StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",customer.getCustomer()), customer.getStatVariable());
//                                String ag = getAvailableAgent();
//                                System.out.println("Master : CHANNEL_PARK : AvailableAgent : "+ag);
//                                customer.setAgent(ag);
                                customer.setLeadAnswer(System.currentTimeMillis());
//                                agents.put(ag, 2);
//                                agentSet.remove(customer.getAgent());
//                                StatsdClient.statsd.recordGaugeValue(String.format("agent_customer_objects,customer=%s,agent=%s",vdu,ag), customer.getStatVariable());
//                                System.out.println("Master : CHANNEL_PARK : Agent Stat : "+agents.get(ag));
//                                System.out.println("Master : CHANNEL_PARK : availabelAgentCount : " + getAvailableAgentCount());
//                                System.out.println("Master : CHANNEL_PARK : agents : " + agents.toString());
//                                customerObjects.put(vdu, customer);
//                                getSender().tell(customer, getSelf());
                                inbound.tell(customer, getSelf());
                            }else{
                                System.out.println("Master : CHANNEL_PARK : Customer not found for Caller-Destination-Number : "+vdu);
                            }
                            //getSender().tell(customerObjects.get(), getSelf());
                        }
                        if(message.getEventName().equalsIgnoreCase("CHANNEL_BRIDGE") && message.getEventHeaders().get("Caller-Direction").equals("outbound")){
                            System.out.printf("Master : CHANNEL_BRIDGE : EVENT : %s %s\n",message.getEventName(),message.getEventHeaders().toString());
                            //String uuid = message.getEventHeaders().get("Unique-ID");
                            //System.out.println("Master : uuid : "+uuid);
                            String vdu = message.getEventHeaders().get("Caller-Caller-ID-Number");
                            System.out.println("Master : CHANNEL_BRIDGE : customer : "+vdu);
                            Customer customer = customerObjects.get(vdu);
                            if(customer != null ){
                                customer.setStatVariable(4);
                                StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",customer.getCustomer()), customer.getStatVariable());
                                customer.setAgentAnswer(System.currentTimeMillis());
                                PERFORMANCE_CUSTOMER_STATS.add(customer.getAgentAnswer() - customer.getLeadAnswer());
                            }else{
                                System.out.println("Master : CHANNEL_BRIDGE : Customer not found for Caller-Caller-ID-Number : "+vdu);
                            }
                            //getSender().tell(customerObjects.get(), getSelf());
                        }
                        if(message.getEventName().equalsIgnoreCase("CHANNEL_HANGUP_COMPLETE") && message.getEventHeaders().get("Caller-Direction").equals("outbound")){
                            System.out.printf("Master : CHANNEL_HANGUP_COMPLETE : EVENT : %s %s\n",message.getEventName(),message.getEventHeaders().toString());
                            //String crdnis = message.getEventHeaders().get("Caller-RDNIS");
                            
//                            String crdnis = message.getEventHeaders().get("Caller-Destination-Number");
                            //System.out.println("Master : CHANNEL_HANGUP_COMPLETE : Channel-HIT-Dialplan : "+message.getEventHeaders().get("Channel-HIT-Dialplan"));
                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : variable_stage : "+message.getEventHeaders().get("variable_stage"));
                            if(!message.getEventHeaders().get("variable_stage").equalsIgnoreCase("originate")){
                                String crdnis = message.getEventHeaders().get("Caller-Caller-ID-Number");
                                System.out.println("Master : CHANNEL_HANGUP_COMPLETE : customer : "+crdnis);
                                Customer customer = customerObjects.get(crdnis);

    //                            String crdnis = message.getEventHeaders().get("variable_call_uuid");
    //                            System.out.println("Master : customer : uuid : "+crdnis);
    //                            Customer customer = getCustomerBasedOnUUID(crdnis);

    //                            long duration = Long.parseLong(message.getEventHeaders().get("variable_mduration"));
    //                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : duration : "+duration);
                                System.out.println("Master : CHANNEL_HANGUP_COMPLETE : availabelAgentCount : " + getAvailableAgentCount());
                                System.out.println("Master : CHANNEL_HANGUP_COMPLETE : agents : " + agents.toString());
                                if(customer != null){
                                    if(!customer.isHangupComplete()){
                                        customer.setHangupComplete(true);
                                        System.out.println("Master : CHANNEL_HANGUP_COMPLETE : customer : Stat Variable : "+customer.getStatVariable());
                                        long ts = System.currentTimeMillis();
                                        customer.setCallEndTS(ts);
                                        long duration = System.currentTimeMillis() - customer.getAgentAnswer();
                                        System.out.println("Master : CHANNEL_HANGUP_COMPLETE : duration : "+duration);
                                        customer.setCallDuration(duration);
                                        if(customer.getStatVariable() > 3){
                                            long t = (long) Math.round(duration * ratio_call_post_call_activity);
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : customer : t : "+t);
                                            customer.setWaitToReset(t);
                                            customer.setExpiryTS(ts + t);
                                            customer.setPostCallEndTS(ts + t);
                                            customer.setStatVariable(11);
                                            StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",customer.getCustomer()), customer.getStatVariable());
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : customer.getAgent() : "+customer.getAgent());
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : availabelAgentCount : " + getAvailableAgentCount());
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : agents : " + agents.toString());
                                            agents.put(customer.getAgent(), 3);
                                            StatsdClient.statsd.recordGaugeValue(String.format("agents,agent=%s",customer.getAgent()), 3);
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : Agent stat : " + agents.get(customer.getAgent()));
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : availabelAgentCount : " + getAvailableAgentCount());
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : agents : " + agents.toString());
                                        }
                                        if(customer.getStatVariable() == 3){
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : customer.getAgent() : "+customer.getAgent());
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : availabelAgentCount : " + getAvailableAgentCount());
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : agents : " + agents.toString());
                                            agents.put(customer.getAgent(), 3);
                                            StatsdClient.statsd.recordGaugeValue(String.format("agents,agent=%s",customer.getAgent()), 3);
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : Agent stat : " + agents.get(customer.getAgent()));
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : availabelAgentCount : " + getAvailableAgentCount());
                                            System.out.println("Master : CHANNEL_HANGUP_COMPLETE : agents : " + agents.toString());
                                            customer.setAgentOriginateSuccess(false);
                                            customer.setStatVariable(5);
                                            StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",customer.getCustomer()), customer.getStatVariable());
                                        }
                                        if(customer.getStatVariable() < 3){
                                            customer.setAgentOriginateSuccess(false);
                                            customer.setStatVariable(7);
                                            StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",customer.getCustomer()), customer.getStatVariable());
                                        }
                                        System.out.println("Master : CHANNEL_HANGUP_COMPLETE : customer stat variable : "+customer.getStatVariable());
                                    }
                                }else{
                                    System.out.println("Master : CHANNEL_HANGUP_COMPLETE : Customer not found for Caller-Caller-ID-Number : "+crdnis);
                                }
                            }else{
                                String crdnis = message.getEventHeaders().get("Caller-Callee-ID-Number");
                                System.out.println("Master : CHANNEL_HANGUP_COMPLETE : customer : "+crdnis);
                                Customer customer = customerObjects.get(crdnis);
                                if(customer != null){
                                    if(!customer.isHangupComplete()){
                                        customerCheckCounter++;
                                        customer.setHangupComplete(true);
                                        customer.setStatVariable(-3);
                                        StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",customer.getCustomer()), customer.getStatVariable());
                                        customer.setLeadAnswer(System.currentTimeMillis());
                                        int check = (int) (N / ANSWERING_PERCENTAGE_COUNTER_STATS.getCount());
                                        if(agentSet.size() > 0 && customerCheckCounter >= check){
                                            agentSet.remove(agentSet.toArray()[0]);
                                            customerCheckCounter = 0;
                                        }
                                    }
                                }else{
                                    System.out.println("Master : CHANNEL_HANGUP_COMPLETE : Customer not found for Caller-Callee-ID-Number : "+crdnis);
                                }
                            }
                            //master.tell(eve, getSelf());
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .matchEquals("RESET_AGENTS", message -> {
                    //System.out.println("Master RESET_AGENTS => " + message);
                    long currentTS = System.currentTimeMillis();
                    ArrayList<String> list = new ArrayList<String>();
                    for (Map.Entry<String, Customer> entry : customerObjects.entrySet()) {
                        String key = entry.getKey();
                        Customer customer = entry.getValue();
                        if(customer.getExpiryTS()!= 0 && customer.getExpiryTS() < currentTS){
                            agents.put(customer.getAgent(), 1);
                            agentPTS.put(customer.getAgent(), System.currentTimeMillis());
                            StatsdClient.statsd.recordGaugeValue(String.format("agents,agent=%s",customer.getAgent()), 1);
                            //agentSet.remove(customer.getAgent());
                            list.add(key);
                        }
                        if(!customer.isAgentOriginateSuccess() && customer.getStatVariable() == 5){
                            agents.put(customer.getAgent(), 1);
                            StatsdClient.statsd.recordGaugeValue(String.format("agents,agent=%s",customer.getAgent()), 1);
                            //agentSet.remove(customer.getAgent());
                            list.add(key);
                        }
                        if(!customer.isAgentOriginateSuccess() && customer.getStatVariable() == 7){
                            list.add(key);
                        }
                        if(customer.getStatVariable() < 0){
                            list.add(key);
                        }
//                        if(!customer.isAgentOriginateSuccess() && customer.getStatVariable() == 3){
//                            agents.put(customer.getAgent(), 1);
//                            agentSet.remove(customer.getAgent());
//                            list.add(key);
//                        }
//                        if(!customer.isAgentOriginateSuccess() && customer.getStatVariable() < 3){
//                            list.add(key);
//                        }
                    }
                    for (int i = 0; i < list.size(); i++) {
                        Customer call = customerObjects.get(list.get(i));
                        ANSWERING_DURATION_STATS.add(call.getLeadAnswer() - call.getLeadOriginate());
                        if(call.getStatVariable() > 3){
                            CALL_HANDLING_DURATION_STATS.add(call.getPostCallEndTS() - call.getAgentAnswer());
                        }
                        if(call.getStatVariable()>=2){
                            ANSWERING_PERCENTAGE_COUNTER_STATS.add(1);
                        }
                        N++;
                        customerObjects.remove(list.get(i));
                    }
                    
                    if(getAvailableCustomerCount() <= 0){
                        agentSet.clear();
                    }
                    
                    for (Map.Entry<String, Integer> entry : agents.entrySet()) {
                        String key = entry.getKey();
                        Integer value = entry.getValue();
                        StatsdClient.statsd.recordGaugeValue(String.format("agents,agent=%s",key), value);
                    }
                    
                    for (Map.Entry<String, Integer> entry : customers.entrySet()) {
                        String key = entry.getKey();
                        Integer value = entry.getValue();
                        StatsdClient.statsd.recordGaugeValue(String.format("customers,customer=%s",key), value);
                    }
                    
                    for (Map.Entry<String, Customer> entry : customerObjects.entrySet()) {
                        String key = entry.getKey();
                        Customer customer = entry.getValue();
                        StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",key), customer.getStatVariable());
                    }
                    
                    pCounter++;
                    if(pCounter == 5){
                        String str = "Customer Waiting Time(in ms)";
                        StatsdClient.statsd.recordGaugeValue(String.format("performance,param=%s",str), PERFORMANCE_CUSTOMER_STATS.getMean());
                        str = "Agent Waiting Time(in ms)";
                        StatsdClient.statsd.recordGaugeValue(String.format("performance,param=%s",str), PERFORMANCE_AGENT_STATS.getMean());
                        pCounter = 0;
                    }
                })
                .matchEquals("INIT_CUSTOMER", message -> {
                    System.out.println("Master INIT_CUSTOMER => " + message);
                    int availableAgentCount = getAvailableAgentCount();
                    
                    if(parkCustomers.size() > 0){
                        for(int i=0;i<availableAgentCount;i++){
                            String vdu = parkCustomers.poll();
                            Customer customer = customerObjects.get(vdu);
                            if(customer != null){
                                //System.out.println("Master : INIT_CUSTOMER : customer : Stat Variable : "+customer.getStatVariable());
                                //customer.setUuid(uuid);
                                customer.setStatVariable(3);
                                StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",customer.getCustomer()), customer.getStatVariable());
                                String ag = getAvailableAgent();
                                if(!ag.isEmpty()){
                                    System.out.println("Master : INIT_CUSTOMER : AvailableAgent : "+ag);
                                    customer.setAgent(ag);
                                    //customer.setLeadAnswer(System.currentTimeMillis());
                                    agents.put(ag, 2);
                                    StatsdClient.statsd.recordGaugeValue(String.format("agents,agent=%s",ag), 2);
                                    agentSet.remove(customer.getAgent());
                                    StatsdClient.statsd.recordGaugeValue(String.format("agent_customer_objects,customer=%s,agent=%s",vdu,ag), customer.getStatVariable());
                                    System.out.println("Master : INIT_CUSTOMER : Agent Stat : "+agents.get(ag));
                                    System.out.println("Master : INIT_CUSTOMER : availabelAgentCount : " + getAvailableAgentCount());
                                    System.out.println("Master : INIT_CUSTOMER : agents : " + agents.toString());
                                    customerObjects.put(vdu, customer);

                                    if(agentPTS.get(ag) > 0){
                                        PERFORMANCE_AGENT_STATS.add(System.currentTimeMillis() - agentPTS.get(ag));
                                        agentPTS.put(ag, 0L);
                                    }

                                    inbound.tell(customer, getSelf());
                                }else{
                                    parkCustomers.add(vdu);
                                }
                            }else{
                                System.out.println("Master : INIT_CUSTOMER : Customer not found for Caller-Destination-Number : "+vdu);
                            }
                            if(parkCustomers.size() == 0)
                                break;
                        }
                    }
                    
                    availableAgentCount = getPredectiveAvailableAgentCount();
                    System.out.println("Master : agents : " + agents.toString());
                    //System.out.println("Master : customers : " + customers.toString());
                    if(nThresholdCounter < nThreshold){
                        System.out.println("Master INIT_CUSTOMER => availabelAgentCount : " + availableAgentCount);
                        if(availableAgentCount > 0)
                            for (Map.Entry<String, Integer> entry : customers.entrySet()) {
                                String key = entry.getKey();
                                Integer value = entry.getValue();
                                if(availableAgentCount == 0)
                                    break;
                                if(value == 1 && availableAgentCount > 0){
                                    Customer customer = new Customer(key);
                                    customerObjects.put(key, customer);
                                    StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",customer.getCustomer()), customer.getStatVariable());
                                    inbound.tell(customerObjects.get(key), getSelf());
                                    customers.put(key, 2);
                                    StatsdClient.statsd.recordGaugeValue(String.format("customers,customer=%s",key), 2);
                                    availableAgentCount--;
                                    nThresholdCounter++;
                                    //break;
                                }
                            }
                        
                    }else{
                        //availableAgentCount = getPredectiveAvailableAgentCount();
                        System.out.println("Master INIT_CUSTOMER => predectiveAvailableAgentCount : " + availableAgentCount);
                        if(availableAgentCount > 0)
                            for (Map.Entry<String, Integer> entry : customers.entrySet()) {
                                String key = entry.getKey();
                                Integer value = entry.getValue();
                                if(availableAgentCount == 0)
                                    break;
                                if(value == 1 && availableAgentCount > 0){
                                    Customer customer = new Customer(key);
                                    customerObjects.put(key, customer);
                                    StatsdClient.statsd.recordGaugeValue(String.format("customer_objects,customer=%s",customer.getCustomer()), customer.getStatVariable());
                                    inbound.tell(customerObjects.get(key), getSelf());
                                    customers.put(key, 2);
                                    StatsdClient.statsd.recordGaugeValue(String.format("customers,customer=%s",key), 2);
                                    availableAgentCount--;
                                    //break;
                                }
                            }
                    }
                })
                .match(ActorRef.class, ar -> {
                    try{
                        System.out.println("Master ActorRef.class => " + ar.path().toString());
                        inbound = ar;
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                })
                .match(InetSocketAddress.class, addr -> {
                    System.out.println("Master InetSocketAddress.class => " + addr.toString());
                })
                .match(ByteString.class, message -> {
                    System.out.println("Master ByteString.class => " + message);
                })
                .matchAny(message -> {
                    System.out.println("Master Any Msg => " + message);
                })
                .build();
    }
    
    public int getAvailableCustomerCount(){
        int c = 0;
        for (Map.Entry<String, Integer> entry : customers.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            if(value == 1)
                c++;
        }
        return c;
    }
    
    public int getOriginateStateCustomerCount(){
        int c = 0;
        for (Map.Entry<String, Customer> entry : customerObjects.entrySet()) {
            String key = entry.getKey();
            Customer customer = entry.getValue();
            if(customer.getStatVariable() == 1)
                c++;
        }
        return c;
    }
    
    public int getTransferedStateCustomerCount(){
        int c = 0;
        for (Map.Entry<String, Customer> entry : customerObjects.entrySet()) {
            String key = entry.getKey();
            Customer customer = entry.getValue();
            if(customer.getStatVariable() >= 3 && customer.getStatVariable() < 5)
                c++;
        }
        return c;
    }
    
    public String getAvailableAgent(){
        String agent = "";
        for (String ags : agentSet) {
            for (Map.Entry<String, Integer> entry : agents.entrySet()) {
                String key = entry.getKey();
                Integer value = entry.getValue();
                if(value == 1 && ags == key){
                    agent = key;
                    break;
                }
            }   
            if(!agent.isEmpty()){
                agentSet.remove(agent);
                break;
            }
        }
        
        return agent;
    }
    
    public int getAvailableAgentCount(){
        int c = 0;
        for (Map.Entry<String, Integer> entry : agents.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            if(value == 1)
                c++;
        }
        return c;
    }
    
    public Set<String> getAvailableAgents(){
        Set<String> ags = new HashSet<String>();
        for (Map.Entry<String, Integer> entry : agents.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            if(value == 1)
                ags.add(key);
        }
        return ags;
    }
    
    public int getPredectiveAvailableAgentCount(){
        int c = 0;
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=GLOBAL_AGENT_SET_SIZE"), agentSet.size());
        
        System.out.println("\n\n\n\n\nPredectiveAvailableAgentCount : Time : "+new Date());
        System.out.println("PredectiveAvailableAgentCount : agentSet : "+agentSet.toString());
        System.out.println("PredectiveAvailableAgentCount : CALL_HANDLING_DURATION_STATS : "+CALL_HANDLING_DURATION_STATS.getMean());
        System.out.println("PredectiveAvailableAgentCount : ANSWERING_DURATION_STATS : "+ANSWERING_DURATION_STATS.getMean());
        System.out.println("PredectiveAvailableAgentCount : ANSWERING_PERCENTAGE_COUNTER_STATS : "+ANSWERING_PERCENTAGE_COUNTER_STATS.getCount());
        System.out.println("PredectiveAvailableAgentCount : N : "+N);
        System.out.println("PredectiveAvailableAgentCount : ANSWERING_PERCENTAGE : "+(N / ANSWERING_PERCENTAGE_COUNTER_STATS.getCount()));
        
        double duration = CALL_HANDLING_DURATION_STATS.getMean() - ANSWERING_DURATION_STATS.getMean();
        System.out.println("PredectiveAvailableAgentCount : duration : "+duration);
        int fCounter = 0; 
        
        for (Map.Entry<String, Customer> entry : customerObjects.entrySet()) {
            String key = entry.getKey();
            Customer customer = entry.getValue();
            int stat = customer.getStatVariable();
            if(stat >= 4 && stat != 5 && stat != 7){
                double diff = System.currentTimeMillis() - customer.getAgentAnswer();
                System.out.println("PredectiveAvailableAgentCount : customer : "+customer.getCustomer());
                System.out.println("PredectiveAvailableAgentCount : diff : "+diff);
                if(duration > 0 && diff > duration){
                    if(!agentSet.contains(customer.getAgent())){
                        agentSet.add(customer.getAgent());
                        fCounter++;
                    }
                }
            }
        }
        int cCounter = fCounter;
        int aCounter = 0;
        
        Set<String> aagent = getAvailableAgents();
        if(aagent.size() > 0){
            for (String agent : aagent) {
                if(!agentSet.contains(agent)){
                    agentSet.add(agent);
                    fCounter++;
                    aCounter++;
                }
            }
        }
        
        if(fCounter > 0 && N > 0 && ANSWERING_PERCENTAGE_COUNTER_STATS.getCount() > 0){
            c = (int) (fCounter * N / ANSWERING_PERCENTAGE_COUNTER_STATS.getCount());
        }else{
            c = (int) fCounter;
        }
        
        //StatsdClient.statsd.recordGaugeValue(String.format("predective_var,CALL_HANDLING_DURATION=%s,ANSWERING_DURATION=%s,DURATION=%s,ANSWERING_PERCENTAGE_COUNTER=%s,N=%s,ANSWERING_PERCENTAGE=%s,GLOBAL_AGENT_SET_SIZE=%s,AVAILABLE_AGENT_SET_SIZE=%s,PREDECTIVE_AGENT_COUNTER=%s,PREDECTIVE_OBJECT_COUNTER=%s",CALL_HANDLING_DURATION_STATS.getMean(),ANSWERING_DURATION_STATS.getMean(),duration,ANSWERING_PERCENTAGE_COUNTER_STATS.getCount(),N,(N / ANSWERING_PERCENTAGE_COUNTER_STATS.getCount()),agentSet.size(),aagent.size(),cCounter,aCounter), c);
        
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=CALL_HANDLING_DURATION"), CALL_HANDLING_DURATION_STATS.getMean());
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=ANSWERING_DURATION"), ANSWERING_DURATION_STATS.getMean());
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=DURATION"), duration);
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=ANSWERING_PERCENTAGE_COUNTER"), ANSWERING_PERCENTAGE_COUNTER_STATS.getCount());
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=N"), N);
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=ANSWERING_PERCENTAGE"), (N / ANSWERING_PERCENTAGE_COUNTER_STATS.getCount()));
        
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=AVAILABLE_AGENT_SET_SIZE"), aagent.size());
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=PREDECTIVE_COUNTER_A"), aCounter);
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=PREDECTIVE_COUNTER_B"), cCounter);
        
        
        
        if(c > 0 && parkCustomers.size() > 0){
            c = c - parkCustomers.size();
            if(c < 0)
                c = 0;
        }
        
        int parkedQueueCount = parkCustomers.size();
        int originateCount = getOriginateStateCustomerCount();
        int transferedCount = getTransferedStateCustomerCount();
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=PARKED_CUSTOMERS"), parkedQueueCount);
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=ORIGINATE_CUSTOMERS"), originateCount);
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=TRANSFERED_CUSTOMERS"), transferedCount);
//        if((parkedQueueCount + originateCount) == 0){
//            if(c < agentSet.size()){
//                c = agentSet.size();//c + (agentSet.size() - c);
//            }
//        }
        int temp = agentSet.size() - (parkedQueueCount + originateCount);
        if(temp > 0)
            c = c + temp;
        
        if(c > agents.size())
            c = agents.size();
        
        if(c > getAvailableCustomerCount()){
            c = getAvailableCustomerCount();
        }
        
        StatsdClient.statsd.recordGaugeValue(String.format("predective_var,param=PredectiveAvailableAgentCount"), c);
        
        return c;
    }
    
    public Customer getCustomerBasedOnUUID(String uuid){
        Customer customer = null;
        for (Map.Entry<String, Customer> entry : customerObjects.entrySet()) {
            String key = entry.getKey();
            Customer value = entry.getValue();
            if(value.getUuid().equals(uuid)){
                customer = value;
                break;
            }
        }
        return customer;
    }
}
