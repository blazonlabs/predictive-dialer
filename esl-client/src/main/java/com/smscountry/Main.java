package com.smscountry;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author neerajnagi
 */
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.io.Tcp;
import com.smscountry.broker.MqttServer;
import com.typesafe.config.Config;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String args[]){
        
        Config conf = Util.conf();
        int outPort = conf.getInt("outbound.port");
        int inPort = conf.getInt("inbound.port");
        String inIp = conf.getString("inbound.ip");
        
        ActorSystem system = ActorSystem.create("predictive-dialer");
        
        final ActorRef tcpManager = Tcp.get(system).manager();
        Util.allConfig();
        ActorRef outbound = system.actorOf(Props.create(OutboundSocket.class, tcpManager, outPort));
        
        ActorRef inbound = system.actorOf(Props.create(InboundSocket.class, tcpManager, inIp, inPort));
        
        ActorRef master = system.actorOf(Props.create(Master.class));
        master.tell(inbound, ActorRef.noSender());
        ActorRef mqttbroker = system.actorOf(Props.create(MqttServer.class));
        mqttbroker.tell(master, ActorRef.noSender());
        mqttbroker.tell("START_MQTT_BROKER", ActorRef.noSender());
        
        inbound.tell(master, ActorRef.noSender());
        
        Logger.getLogger("io.moquette.server.netty.AutoFlushHandler").setLevel(Level.OFF);

        
        
    }
}
